#include <stdio.h>
#include "nano_stopwatch.h"

int main() {
    nsw_t nsw = nsw_init();
    usleep(1000);
    printf("elapsed us: %f\n", nsw_elapsed_us(&nsw));
    return 0;
}
