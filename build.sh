#!/bin/bash
BUILD_TYPE=release
TARGET=all
if [ $# -eq 1 ]; then
    if [[ $1 != "debug" && $1 != "release" ]]; then
        TARGET=$1
    else
        BUILD_TYPE=$1
    fi
elif [ $# -eq 2 ]; then
    if [[ $1 != "debug" && $1 != "release" ]]; then
        echo "error param! please input 'debug' or 'release'."
        exit 1
    fi
    BUILD_TYPE=$1
    TARGET=$2
fi

WORKING_DIR=$(cd -P -- "$(dirname -- "$0")" && pwd)
BUILD_DIR=$WORKING_DIR/$BUILD_TYPE
echo "$BUILD_DIR"
cmake -S "$WORKING_DIR" -B "$BUILD_DIR" -DCMAKE_BUILD_TYPE="$BUILD_TYPE"
cmake --build "$BUILD_DIR" --target "$TARGET" -j 8
