# nano_stopwatch

一个纳秒精度的计时器，支持Windows和Linux。

## 示例
```c
#include <stdio.h>
#include "nano_stopwatch.h"

int main() {
	nsw_t nsw = nsw_init();
	printf("elapsed ns: %.2f\n", nsw_elapsed_ns(&nsw));
	printf("cpu_cycle = %f\n", __nano_stopwatch_cpu_freq_per_ns);
#ifdef _WIN32
	Sleep(1000);
#else
	usleep(1000000);
#endif // _WIN32
	printf("elapsed us: %.2f\n", nsw_elapsed_us(&nsw));
	return 0;
}

输出：
elapsed ns: 20.56
cpu_cycle = 3.696000
elapsed us: 1013685.23
```
